# cache-scheduler

Provides implementation for a cache

1. Cache: Either returns a cached value or lookup using a provided supplier in a concurrent and thread safe manner

2. DeadlineEngine: Schedule task based on the provided deadline and execute them concurrently

