package com.scb;

public interface Cache<K, V> {
    V get(K key);
}
