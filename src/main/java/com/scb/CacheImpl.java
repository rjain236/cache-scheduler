package com.scb;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Implementation of a blocking cache inspired by concurrent hashmap, where we acquire locks on a
 * bucket.
 */
public class CacheImpl<K, V> implements Cache<K, V> {

  private final int maxConcurrency;
  private final Function<K, V> supplier;
  private Segment<K, V>[] segments;

  /**
   * @param supplier lookup function, can return null. Exceptions are expected to be handled by the
   *     downstream
   * @param maxConcurrency maximum number of threads which can access cache simultaneously, if all
   *     are accessing different segments
   */
  public CacheImpl(Function<K, V> supplier, int maxConcurrency) {
    if (supplier == null) {
      throw new IllegalArgumentException("Cannot have null supplier");
    }
    this.supplier = supplier;
    this.maxConcurrency = maxConcurrency;
    this.initialize();
  }

  /**
   * Get the value from the cache against the given key or lookup the value for the key from
   * provided supplier
   *
   * @param key cannot be null
   * @return a value against the key, either cached or looked up (will return null if look up return
   *     no value)
   */
  @Override
  public V get(K key) {
    if (key == null) {
      throw new NullPointerException("Key cannot be null");
    }
    Segment<K, V> segment = lookupSegment(key);
    Optional<V> lookupValue = segment.get(key);
    if (lookupValue == null) {
      // synchronize on the segment
      synchronized (segment) {
        // to make sure no other thread had added the value in the meantime
        Optional<V> checkValueAgain = segment.get(key);
        if (checkValueAgain == null) {
          V calculated = supplier.apply(key);
          segment.put(key, calculated);
          return calculated;
        } else {
          return checkValueAgain.orElse(null);
        }
      }
    } else {
      // return null value to represent no value for provided key after supplier evaluation
      return lookupValue.orElse(null);
    }
  }

  @SuppressWarnings("unchecked")
  private void initialize() {
    this.segments =
        Stream.generate(this::createSegment).limit(maxConcurrency).toArray(Segment[]::new);
    for (int i = 0; i < maxConcurrency; i++) {
      this.segments[i] = createSegment();
    }
  }

  private Segment<K, V> createSegment() {
    return new Segment<>();
  }

  private Segment<K, V> lookupSegment(K key) {
    int hash = getHash(key);
    return segments[hash % maxConcurrency];
  }

  private int getHash(K key) {
    int hash = key.hashCode();
    // just fudging the hashcode for the key so that lookup in segment can be
    // more efficient, we can look at better strategies like to generate
    // different hashcode than the one being used inside hashmap
    return Objects.hashCode(hash * hash);
  }

  private static class Segment<K, V> {
    // it won't be efficient hashing as keys with same modulo will come to this map
    // we can change the hashing strategy for determining the segments
    // but would still be better than using a simple collection
    private Map<K, Optional<V>> data = new HashMap<>();

    Optional<V> get(K key) {
      return data.get(key);
    }

    public void put(K key, V value) {
      data.put(key, Optional.ofNullable(value));
    }
  }
}
