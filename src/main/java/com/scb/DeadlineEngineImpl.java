package com.scb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

public class DeadlineEngineImpl implements DeadlineEngine {

  private static final Logger LOGGER = LoggerFactory.getLogger(DeadlineEngineImpl.class);

  // atomic as schedule(long deadlineMs) can be called from multiple threads
  private final AtomicLong requestIdGenerator = new AtomicLong(0L);
  private final ForkJoinPool executor = new ForkJoinPool(4);

  private final Map<Long, Long> requests = new ConcurrentHashMap<>();

  @Override
  public long schedule(long deadlineMs) {
    long reqId = requestIdGenerator.getAndIncrement();
    requests.put(reqId, deadlineMs);
    return reqId;
  }

  @Override
  public boolean cancel(long requestId) {
    AtomicBoolean cancelled = new AtomicBoolean(false);
    requests.computeIfPresent(
        requestId,
        (reqId, deadline) -> {
          LOGGER.info("cancelling request for {}", reqId);
          cancelled.set(true);
          // return null to remove the entry atomically after setting cancelled to true
          return null;
        });
    return cancelled.get();
  }

  @Override
  public int poll(long nowMs, Consumer<Long> handler, int maxPoll) {
    Iterator<Map.Entry<Long, Long>> iterator = requests.entrySet().iterator();
    List<Task> tasks = new ArrayList<>(maxPoll);
    AtomicInteger count = new AtomicInteger(0);
    // there is a chance that concurrently new requests are added which are not yet visible to
    // iterator in that case we might fire less number of events than maxPoll, but those requests
    // would be picked in the next iteration
    while (iterator.hasNext()) {
      Map.Entry<Long, Long> next = iterator.next();
      if (next.getValue() < nowMs) {
        // trigger the event
        Long requestId = next.getKey();
        // as traversing over concurrent hashmap is weekly consistent
        // we need to make sure while scheduling we check and schedule the
        // request atomically, would have some performance penalty
        requests.computeIfPresent(
            requestId,
            (reqId, deadline) -> {
              LOGGER.info("Triggering workflow for req id {}", reqId);
              // if it is still present schedule it
              tasks.add(new Task(reqId, handler));
              // increment the count
              count.incrementAndGet();
              // remove from the map
              return null;
            });
        if (count.get() == maxPoll) {
          // we have created max number of tasks
          break;
        }
      }
    }
    // invoke all the tasks, we can add a timeout if that is the requirement
    executor.invokeAll(tasks);

    // we can return more information around the task that got failed
    return tasks.stream().mapToInt(task -> task.getSuccess() ? 1 : 0).sum();
  }

  @Override
  public int size() {
    return requests.size();
  }

  private static class Task implements Callable<Boolean> {

    private final long requestId;
    private final Consumer<Long> handler;
    // need to be volatile read across threads
    private volatile boolean success = false;

    public Task(long requestId, Consumer<Long> handler) {
      this.requestId = requestId;
      this.handler = handler;
    }

    public boolean getSuccess() {
      return success;
    }

    @Override
    public Boolean call() {
      try {
        handler.accept(requestId);
        success = true;
      } catch (RuntimeException ex) {
        // make sure we catch runtime exceptions
        LOGGER.error("Error occurred for request id {}", requestId, ex);
      }
      return success;
    }
  }
}
