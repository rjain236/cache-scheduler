package com.scb;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class CacheImplTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(CacheImplTest.class);

  private final ExecutorService executors = Executors.newFixedThreadPool(10);

  private final Function<Integer, Integer> delayedSquarer =
      (num) -> {
        LOGGER.info("Calculating value {}", num);
        try {
          // simulate delay
          Thread.sleep(300);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        return num * num;
      };

  @Test
  public void testCache() throws InterruptedException {
    Function<Integer, Integer> supplier = getSupplier(delayedSquarer);
    Cache<Integer, Integer> cache = new CacheImpl<>(supplier, 4);
    List<Callable<Integer>> tasks =
        IntStream.range(0, 10)
            .mapToObj(
                i ->
                    (Callable<Integer>)
                        () -> {
                          Integer value = cache.get(3);
                          LOGGER.info("Calculated value {}", value);
                          assertEquals(9, (int) value);
                          return value;
                        })
            .collect(Collectors.toList());
    executors.invokeAll(tasks);
    Mockito.verify(supplier, Mockito.times(1)).apply(3);
  }

  @Test
  public void nullKeyThrowsException() {
    Function<Integer, Integer> supplier = getSupplier(delayedSquarer);
    Cache<Integer, Integer> cache = new CacheImpl<>(supplier, 4);
    assertThrows(NullPointerException.class, () -> cache.get(null));
  }

  @Test
  public void testNoValueReturnedFromSupplier() {
    Function<Integer, Integer> supplier = getSupplier((key) -> null);
    Cache<Integer, Integer> cache = new CacheImpl<>(supplier, 4);
    // call multiple times
    assertNull(cache.get(3));
    assertNull(cache.get(3));
    // should be called only once
    Mockito.verify(supplier, Mockito.times(1)).apply(3);
  }

  @Test
  public void testExceptionOccurredInSupplier() {
    Function<Integer, Integer> supplier =
        getSupplier(
            (key) -> {
              if (key == 3) {
                throw new RuntimeException("Exception occurred while lookup!!" + key);
              } else {
                return key;
              }
            });
    Cache<Integer, Integer> cache = new CacheImpl<>(supplier, 4);
    // call multiple times

    assertThrows(RuntimeException.class, () -> cache.get(3));
    assertThrows(RuntimeException.class, () -> cache.get(3));
    assertTrue(4 == cache.get(4));
    assertTrue(4 == cache.get(4));
    // should be called as many times till we are able to run without exception
    // downstream expected to handle exception
    Mockito.verify(supplier, Mockito.times(2)).apply(3);

    // should be called once only
    Mockito.verify(supplier, Mockito.times(1)).apply(4);
  }

  @SuppressWarnings("unchecked")
  private <K, V> Function<K, V> getSupplier(Function<K, V> supplier) {
    Function<K, V> mock = Mockito.mock(Function.class);
    Mockito.doAnswer(
            invocationOnMock -> {
              K key = invocationOnMock.getArgument(0);
              return supplier.apply(key);
            })
        .when(mock)
        .apply(Mockito.any());
    return mock;
  }
}
