package com.scb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class DeadlineEngineImplTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(DeadlineEngineImplTest.class);
  private final Consumer<Long> handler =
      (requestId) -> LOGGER.info("Executed request {}", requestId);

  private DeadlineEngine deadlineEngine;

  @BeforeEach
  public void setup() {
    deadlineEngine = new DeadlineEngineImpl();
  }

  @Test
  public void testScheduler() {
    deadlineEngine.schedule(100);
    // should not lead to any event execution
    assertEquals(0, deadlineEngine.poll(99, handler, 4));

    // should trigger
    assertEquals(1, deadlineEngine.poll(101, handler, 4));

    // nothing remaining to trigger
    assertEquals(0, deadlineEngine.poll(101, handler, 4));
  }

  @Test
  public void testCancellation() {
    long reqId = deadlineEngine.schedule(100);
    assertTrue(deadlineEngine.cancel(reqId));
    assertEquals(0, deadlineEngine.poll(101, handler, 4));
    assertEquals(deadlineEngine.size(), 0);
  }

  @Test
  public void testCancellationAfterExecution() {
    long reqId = deadlineEngine.schedule(100);
    assertEquals(1, deadlineEngine.poll(101, handler, 4));
    assertFalse(deadlineEngine.cancel(reqId));
  }

  @Test
  public void testMaximumParallelExecutions() {
    // schedule 5 tasks
    deadlineEngine.schedule(100);
    deadlineEngine.schedule(100);
    deadlineEngine.schedule(100);
    deadlineEngine.schedule(100);
    deadlineEngine.schedule(100);
    assertEquals(5, deadlineEngine.size());
    assertEquals(2, deadlineEngine.poll(101, handler, 2));
    assertEquals(3, deadlineEngine.size());
    assertEquals(2, deadlineEngine.poll(101, handler, 2));
    deadlineEngine.schedule(101);
    assertEquals(2, deadlineEngine.size());
    assertEquals(2, deadlineEngine.poll(102, handler, 3));
    assertEquals(0, deadlineEngine.size());
  }

  @Test
  public void testCancelledEventsAreNotExecuted() {
    int numberOfRuns = 200;
    List<Long> requests =
        IntStream.range(0, numberOfRuns)
            .mapToObj(i -> deadlineEngine.schedule(100))
            .collect(Collectors.toList());

    ExecutorService executorService = Executors.newSingleThreadExecutor();
    List<Future<Boolean>> cancellations =
        requests.stream()
            .map(reqId -> (Callable<Boolean>) () -> deadlineEngine.cancel(reqId))
            .map(executorService::submit)
            .collect(Collectors.toList());

    int poll =
        IntStream.range(0, numberOfRuns).map(i -> deadlineEngine.poll(101, handler, 1)).sum();
    int cancelledSum =
        cancellations.stream()
            .mapToInt(
                f -> {
                  try {
                    return f.get() ? 1 : 0;
                  } catch (InterruptedException | ExecutionException e) {
                    LOGGER.error("exception occurred while cancelling", e);
                    throw new RuntimeException(e);
                  }
                })
            .sum();
    assertEquals(numberOfRuns, poll + cancelledSum);
  }

  @Test
  public void testExecutionWhereExceptionOccurred() {
    Consumer<Long> exceptionThrower =
        (reqId) -> {
          throw new RuntimeException("Exception!!!" + reqId);
        };

    // add a task
    deadlineEngine.schedule(100);

    int poll = deadlineEngine.poll(101, exceptionThrower, 3);
    // it will return 0 for a failed task
    assertEquals(0, poll);
    // task should be removed after the failure
    assertEquals(0, deadlineEngine.size());
  }
}
